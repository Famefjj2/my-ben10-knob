#include "../lv_examples.h"
#include "../../../lv_drivers/win32drv/win32drv.h"

#if LV_BUILD_EXAMPLES && LV_USE_BTN

static lv_group_t* g;

/**
 * Create a button with a label and react on click event.
 */
 lv_obj_t* label1;
 lv_obj_t* label2;
 lv_obj_t* obj;
 lv_obj_t* btn;
 lv_obj_t* label_btn;

void encoder_event_handler(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    LV_LOG_USER("main page: %d\n", code);

    if (code == LV_EVENT_CLICKED)
    {
        lv_example_get_started_new();
        lv_obj_del(label1);
        lv_obj_del(label2);
        lv_obj_del(label_btn);
        lv_obj_del(btn);
    }

}

void lv_encoder(void)
{
    g = lv_group_create();
    lv_group_set_default(g);

    lv_indev_t* cur_drv = NULL;
    while(true)
    {
        cur_drv = lv_indev_get_next(cur_drv);
        if (!cur_drv){
            break;
        }
        if(cur_drv->driver->type == LV_INDEV_TYPE_ENCODER)
        {
            lv_indev_set_group(cur_drv, g);
        }
    }
}


void lv_mainpage(char destination[8])
{
    lv_encoder();

    //setup base obj
    obj = lv_obj_create(lv_scr_act());
    lv_obj_set_size(obj, 400, 400);
    lv_obj_set_align(obj, LV_ALIGN_CENTER);
    lv_obj_set_pos(obj, 0, 0);

    //setup label1
    label1 = lv_label_create(obj);
    lv_label_set_text(label1, "Current Destination");
    lv_obj_align_to(label1,obj, LV_ALIGN_CENTER, 0, -15);

    //setup label2
    label2 = lv_label_create(obj);
    lv_label_set_text(label2, destination);
    lv_obj_align_to(label2,obj, LV_ALIGN_CENTER, 0, 15);

    //setup button
    btn = lv_btn_create(lv_scr_act());
    lv_obj_align(btn, LV_ALIGN_CENTER, 0, 60);
    lv_obj_set_size(btn, 100, 30);

    label_btn = lv_label_create(btn);
    lv_label_set_text(label_btn, "Select");
    lv_obj_center(label_btn);
    lv_obj_add_event_cb(btn, encoder_event_handler, LV_EVENT_ALL, NULL);

}



#endif
